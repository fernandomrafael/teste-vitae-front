import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link, withRouter } from 'react-router-dom';
import { instanceOf } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';

class ListaClientes extends Component {
  
  constructor(props) {
    super(props);
    this.state = {faturas: [], isLoading: true};
    this.remove = this.remove.bind(this);
  }

  componentDidMount() {
    this.setState({isLoading: false});

    fetch('/api/fatura', {credentials: 'include'})
      .then(response => response.json())
      .then(data => this.setState({faturas: data, isLoading: false}))
      .catch(() => this.props.history.push('/lista-faturas'));
  }

  async remove(id) {
    await fetch(`/api/fatura/${id}`, {
      method: 'DELETE',
      headers: {
        'X-XSRF-TOKEN': this.state.csrfToken,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    }).then(() => {
      let updatedGroups = [...this.state.groups].filter(i => i.id !== id);
      this.setState({groups: updatedGroups});
    });
  }

  render() {
    const {faturas, isLoading} = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    const faturasList = faturas.map(fatura => {
      
      return <tr key={fatura.id}>
        <td style={{whiteSpace: 'nowrap'}}>{fatura.dataCompetencia}</td>
        <td>{fatura.dataVencimento}</td>
        <td>{fatura.dataPagamento}</td>
        <td>{fatura.valorFatura}</td>
        <td>{fatura.celular}</td>
        <td>
          <ButtonGroup>
            <Button size="sm" color="primary" tag={Link} to={"/api/fatura/" + fatura.id}>Edit</Button>
            <Button size="sm" color="danger" onClick={() => this.remove(fatura.id)}>Delete</Button>
          </ButtonGroup>
        </td>
      </tr>
    });

    return (
      <div>
        <AppNavbar/>
        <Container fluid>
          <div className="float-right">
            <Button color="success" tag={Link} to="/api/fatura">Cadastro Fatura</Button>
          </div>
          <h3>Lista de Faturas</h3>
          <Table className="mt-4">
            <thead>
            <tr>
              <th width="15%">Data Compêtencia</th>
              <th width="15%">Data Vencimento</th>
              <th width="15%">Data Pagamento</th>
              <th width="15%">Valor Fatura</th>
              <th width="30%">Cliente</th>
              <th width="10%">Ações</th>
            </tr>
            </thead>
            <tbody>
            {faturasList}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

export default withCookies(withRouter(ListaClientes));