import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link, withRouter } from 'react-router-dom';
import { instanceOf } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';

class ListaClientes extends Component {
  
  constructor(props) {
    super(props);
    this.state = {clientes: [], isLoading: true};
    this.remove = this.remove.bind(this);
  }

  componentDidMount() {
    this.setState({isLoading: false});

    fetch('/api/clientes', {credentials: 'include'})
      .then(response => response.json())
      .then(data => this.setState({clientes: data, isLoading: false}))
      .catch(() => this.props.history.push('/lista-clientes'));
  }

  async remove(id) {
    await fetch(`/api/group/${id}`, {
      method: 'DELETE',
      headers: {
        'X-XSRF-TOKEN': this.state.csrfToken,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    }).then(() => {
      let updatedGroups = [...this.state.groups].filter(i => i.id !== id);
      this.setState({groups: updatedGroups});
    });
  }

  render() {
    const {clientes, isLoading} = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    const clientesList = clientes.map(cliente => {
      
      return <tr key={cliente.id}>
        <td style={{whiteSpace: 'nowrap'}}>{cliente.nome}</td>
        <td>{cliente.dataNascimento}</td>
        <td>{cliente.sexo}</td>
        <td>{cliente.telefone}</td>
        <td>{cliente.celular}</td>
        <td>
          <ButtonGroup>
            <Button size="sm" color="primary" tag={Link} to={"/api/cliente/" + cliente.id}>Edit</Button>
            <Button size="sm" color="danger" onClick={() => this.remove(cliente.id)}>Delete</Button>
          </ButtonGroup>
        </td>
      </tr>
    });

    return (
      <div>
        <AppNavbar/>
        <Container fluid>
          <div className="float-right">
            <Button color="success" tag={Link} to="/api/cliente">Cadastro Cliente</Button>
          </div>
          <h3>Lista de Clientes</h3>
          <Table className="mt-4">
            <thead>
            <tr>
              <th width="30%">Nome Cliente</th>
              <th width="15%">Data de Nascimento</th>
              <th width="15%">Sexo</th>
              <th width="15%">Telefone</th>
              <th width="15%">Celular</th>
              <th width="10%">Ações</th>
            </tr>
            </thead>
            <tbody>
            {clientesList}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

export default withCookies(withRouter(ListaClientes));