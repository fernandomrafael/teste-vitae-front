import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { instanceOf } from 'prop-types';
import { Cookies, withCookies } from 'react-cookie';

class CadastroClientes extends Component {


  emptyItem = {
    nome: '',
    dataNascimento: '',
    sexo: '',
    telefone: '',
    celular: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      item: this.emptyItem
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }



  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let item = { ...this.state.item };
    item[name] = value;
    this.setState({ item });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const { item } = this.state;
    console.log(JSON.stringify(item))
   /* await fetch('api/cliente', {
      method: 'post',
      body: JSON.stringify(item)
    }).then(function (response) {
      return response.json();
    }).then(function (data) {
      console.log('Cadastro com sucesso', data.html_url);
    });*/
  }


  render() {
    const { item } = this.state;
    const title = <h2>{item.id ? 'Editar Cliente' : 'Cadastro Cliente'}</h2>;

    return <div>
      <AppNavbar />
      <Container>
        {title}
        <Form onSubmit={this.handleSubmit}>

          <FormGroup>
            <Label for="dataCompetencia">Nome</Label>
            <Input type="text" name="nome" id="nome" value={item.nome || ''}
              onChange={this.handleChange} autoComplete="nome" maxLength="8" />
          </FormGroup>
          
          
          <div className="row">
          <FormGroup className="col-md-3 mb-3">
            <Label for="dataNascimento">Data Nascimento</Label>
            <Input type="date" name="dataNascimento" id="dataNascimento" value={item.dataNascimento || ''}
              onChange={this.handleChange} autoComplete="address-level1" />
          </FormGroup>
          <FormGroup className="col-md-3 mb-3">
            <Label for="sexo">Sexo</Label>
            <Input type="select" name="sexo" id="sexo">
              <option>Masculino</option>
              <option>Feminino</option>
            </Input>
          </FormGroup>
            <FormGroup className="col-md-3 mb-3">
              <Label for="telefone">Telefone</Label>
              <Input type="number" name="telefone" id="telefone" value={item.telefone || ''}
                onChange={this.handleChange} autoComplete="address-level1" />
            </FormGroup>
            <FormGroup className="col-md-3 mb-3">
              <Label for="celular">Celular</Label>
              <Input type="number" name="celular" id="celular" value={item.celular || ''}
                onChange={this.handleChange} autoComplete="address-level1" />
            </FormGroup>

          </div>
          <FormGroup>
            <Button color="primary" type="submit">Salvar</Button>{' '}
            <Button color="secondary" tag={Link} to="/">Cancelar</Button>
          </FormGroup>
        </Form>
      </Container>
    </div>
  }
}

export default withRouter(CadastroClientes);