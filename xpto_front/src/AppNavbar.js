import React, { Component } from 'react';
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';

export default class AppNavbar extends Component {
  constructor(props) {
    super(props);
    this.state = { isOpen: false };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return <Navbar color="dark" dark expand="md">
      <NavbarBrand tag={Link} to="/">Home</NavbarBrand>
      <NavbarBrand tag={Link} to="/cadastro-clientes">Cadastro Clientes</NavbarBrand>
      <NavbarBrand tag={Link} to="/cadastro-faturas">Cadastro Faturas</NavbarBrand>
      <NavbarBrand tag={Link} to="/lista-clientes">Lista Clientes</NavbarBrand>
      <NavbarBrand tag={Link} to="/lista-faturas">Lista Faturas</NavbarBrand>
      <NavbarToggler onClick={this.toggle} />
    </Navbar>;
  }
}