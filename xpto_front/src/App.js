import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ListaClientes from './ListaClientes';
import ListaFaturasClientes from './ListaFaturasClientes';
import CadastroClientes from './CadastroClientes';
import CadastroFaturas from './CadastroFaturas';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path='/' exact={true} component={CadastroClientes} />
          <Route path='/lista-clientes' exact={true} component={ListaClientes} />
          <Route path='/lista-faturas' exact={true} component={ListaFaturasClientes} />
          <Route path='/cadastro-clientes' exact={true} component={CadastroClientes} />
          <Route path='/cadastro-faturas' exact={true} component={CadastroFaturas} />
        </Switch>
      </Router>
    )
  }
}

export default App;