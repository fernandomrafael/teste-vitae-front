import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { instanceOf } from 'prop-types';
import { Cookies, withCookies } from 'react-cookie';

class CadastroFaturas extends Component {


  emptyItem = {
    dataCompetencia: '',
    dataPagamento: '',
    dataVencimento: '',
    valorFatura: '',
    cliente: ''
  };

  constructor(props) {
    super(props);
    const { cookies } = props;
    this.state = {
      item: this.emptyItem
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }



  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let item = { ...this.state.item };
    item[name] = value;
    this.setState({ item });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const { item } = this.state;
    console.log(JSON.stringify(item))
   /* await fetch('/api/fatura', {
      method: 'post',
      body: JSON.stringify(item)
    }).then(function (response) {
      return response.json();
    }).then(function (data) {
      console.log('Cadastro com sucesso', data.html_url);
    });*/
  }

  render() {
    const { item } = this.state;
    const title = <h2>{item.id ? 'Editar Fatura' : 'Cadastro Fatura'}</h2>;

    return <div>
      <AppNavbar />
      <Container>
        {title}
        <Form onSubmit={this.handleSubmit}>
          <div className="row">
            <FormGroup className="col-md-4 mb-3">
              <Label for="dataCompetencia">Data Competência</Label>
              <Input type="date" name="dataCompetencia" id="dataCompetencia" value={item.dataCompetencia || ''}
                onChange={this.handleChange} autoComplete="dataCompetencia" maxLength="8" />
            </FormGroup>
            <FormGroup className="col-md-5 mb-3">
              <Label for="address">Data Vencimento</Label>
              <Input type="date" name="dataVencimento" id="dataVencimento" value={item.dataVencimento || ''}
                onChange={this.handleChange} autoComplete="address-level1" />
            </FormGroup>
            <FormGroup className="col-md-3 mb-3">
              <Label for="dataPagamento">Data Pagamento</Label>
              <Input type="date" name="dataPagamento" id="city" value={item.dataPagamento || ''}
                onChange={this.handleChange} autoComplete="address-level1" />
            </FormGroup>
          </div>
          <div className="row">
            <FormGroup className="col-md-4 mb-3">
              <Label for="valorFatura">Valor Fatura</Label>
              <Input type="number" name="valorFatura" id="valorFatura" value={item.valorFatura || ''}
                onChange={this.handleChange} autoComplete="address-level1" />
            </FormGroup>
            <FormGroup className="col-md-5 mb-3">
              <Label for="cliente">Cliente</Label>
              <Input type="text" name="cliente" id="cliente" value={item.cliente || ''}
                onChange={this.handleChange} autoComplete="address-level1" />
            </FormGroup>

          </div>
          <FormGroup>
            <Button color="primary" type="submit">Salvar</Button>{' '}
            <Button color="secondary" tag={Link} to="/">Cancelar</Button>
          </FormGroup>
        </Form>
      </Container>
    </div>
  }
}

export default withRouter(CadastroFaturas);